FROM node:12 as builder

WORKDIR /src/app

COPY package.json package-lock.json ./

RUN npm install --force && npm cache clean --force

COPY . .

ARG WIKIBASE_SCHEME
ARG WIKIBASE_ADDRESS
ARG WIKIBASE_CONCEPT
ARG WIKIBASE_NAME
ARG CUSTOM_CONFIG_QUERY

# Set them as environment variables
ENV WIKIBASE_SCHEME=$WIKIBASE_SCHEME
ENV WIKIBASE_ADDRESS=$WIKIBASE_ADDRESS
ENV WIKIBASE_CONCEPT=$WIKIBASE_CONCEPT
ENV WIKIBASE_NAME=$WIKIBASE_NAME
ENV CUSTOM_CONFIG_QUERY=$CUSTOM_CONFIG_QUERY

RUN npm run-script build

FROM nginx:1-alpine
LABEL org.opencontainers.image.source="https://github.com/rhizomedotorg/artbase-query-gui/"

ADD ./docker/nginx.default.conf /etc/nginx/conf.d/default.conf
COPY --from=builder --chown=nginx:nginx /src/app/build /usr/share/nginx/html