#!/bin/bash
# generate RdfNamespaces.js for custom-wdqs.

echo "
var wikibase = window.wikibase || {};
wikibase.queryService = wikibase.queryService || {};
wikibase.queryService.RdfNamespaces = {};

( function ( $, RdfNamespaces ) {
	\"use strict\";

	RdfNamespaces.NAMESPACE_SHORTCUTS = {
		TIB: {
			tib: \"${WIKIBASE_SCHEME}://${WIKIBASE_CONCEPT}/entity/\",
			tibt: \"${WIKIBASE_SCHEME}://${WIKIBASE_CONCEPT}/prop/direct/\",
			tibtn: \"${WIKIBASE_SCHEME}://${WIKIBASE_CONCEPT}/prop/direct-normalized/\",
			tibs: \"${WIKIBASE_SCHEME}://${WIKIBASE_CONCEPT}/entity/statement/\",
			tibp: \"${WIKIBASE_SCHEME}://${WIKIBASE_CONCEPT}/prop/\",
			tibref: \"${WIKIBASE_SCHEME}://${WIKIBASE_CONCEPT}/reference/\",
			tibv: \"${WIKIBASE_SCHEME}://${WIKIBASE_CONCEPT}/value/\",
			tibps: \"${WIKIBASE_SCHEME}://${WIKIBASE_CONCEPT}/prop/statement/\",
			tibpsv: \"${WIKIBASE_SCHEME}://${WIKIBASE_CONCEPT}/prop/statement/value/\",
			tibpsn: \"${WIKIBASE_SCHEME}://${WIKIBASE_CONCEPT}/prop/statement/value-normalized/\",
			tibpq: \"${WIKIBASE_SCHEME}://${WIKIBASE_CONCEPT}/prop/qualifier/\",
			tibpqv: \"${WIKIBASE_SCHEME}://${WIKIBASE_CONCEPT}/prop/qualifier/value/\",
			tibpqn: \"${WIKIBASE_SCHEME}://${WIKIBASE_CONCEPT}/prop/qualifier/value-normalized/\",
			tibpr: \"${WIKIBASE_SCHEME}://${WIKIBASE_CONCEPT}/prop/reference/\",
			tibprv: \"${WIKIBASE_SCHEME}://${WIKIBASE_CONCEPT}/prop/reference/value/\",
			tibprn: \"${WIKIBASE_SCHEME}://${WIKIBASE_CONCEPT}/prop/reference/value-normalized/\",
			tibno: \"${WIKIBASE_SCHEME}://${WIKIBASE_CONCEPT}/prop/novalue/\",
			tibdata: \"${WIKIBASE_SCHEME}://${WIKIBASE_CONCEPT}/wiki/Special:EntityData/\",
		},
		Wikidata: {
			wikibase: \"http://wikiba.se/ontology#\",
			wd: \"http://www.wikidata.org/entity/\",
			wdt: \"http://www.wikidata.org/prop/direct/\",
			wdtn: \"http://www.wikidata.org/prop/direct-normalized/\",
			wds: \"http://www.wikidata.org/entity/statement/\",
			p: \"http://www.wikidata.org/prop/\",
			wdref: \"http://www.wikidata.org/reference/\",
			wdv: \"http://www.wikidata.org/value/\",
			ps: \"http://www.wikidata.org/prop/statement/\",
			psv: \"http://www.wikidata.org/prop/statement/value/\",
			psn: \"http://www.wikidata.org/prop/statement/value-normalized/\",
			pq: \"http://www.wikidata.org/prop/qualifier/\",
			pqv: \"http://www.wikidata.org/prop/qualifier/value/\",
			pqn: \"http://www.wikidata.org/prop/qualifier/value-normalized/\",
			pr: \"http://www.wikidata.org/prop/reference/\",
			prv: \"http://www.wikidata.org/prop/reference/value/\",
			prn: \"http://www.wikidata.org/prop/reference/value-normalized/\",
			wdno: \"http://www.wikidata.org/prop/novalue/\",
			wdata: \"http://www.wikidata.org/wiki/Special:EntityData/\",
			wdqs: \"https://query.wikidata.org/sparql\"
		},
		W3C: {
			rdfs: \"http://www.w3.org/2000/01/rdf-schema#\",
			rdf: \"http://www.w3.org/1999/02/22-rdf-syntax-ns#\",
			owl: \"http://www.w3.org/2002/07/owl#\",
			skos: \"http://www.w3.org/2004/02/skos/core#\",
			xsd: \"http://www.w3.org/2001/XMLSchema#\",
			prov: \"http://www.w3.org/ns/prov#\",
			ontolex: \"http://www.w3.org/ns/lemon/ontolex#\"
		},
		\"Social/Other\": {
			schema: \"http://schema.org/\",
			geo: \"http://www.opengis.net/ont/geosparql#\",
			geof: \"http://www.opengis.net/def/geosparql/function/\",
			dct: \"http://purl.org/dc/terms/\"
		},
		Blazegraph: {
			bd: \"http://www.bigdata.com/rdf#\",
			bds: \"http://www.bigdata.com/rdf/search#\",
			gas: \"http://www.bigdata.com/rdf/gas#\",
			hint: \"http://www.bigdata.com/queryHints#\"
		}
	};

	RdfNamespaces.ENTITY_TYPES = {
		\"${WIKIBASE_SCHEME}://${WIKIBASE_CONCEPT}/prop/direct/\": \"property\",
		\"${WIKIBASE_SCHEME}://${WIKIBASE_CONCEPT}/prop/direct-normalized/\": \"property\",
		\"${WIKIBASE_SCHEME}://${WIKIBASE_CONCEPT}/prop/\": \"property\",
		\"${WIKIBASE_SCHEME}://${WIKIBASE_CONCEPT}/prop/novalue/\": \"property\",
		\"${WIKIBASE_SCHEME}://${WIKIBASE_CONCEPT}/prop/statement/\": \"property\",
		\"${WIKIBASE_SCHEME}://${WIKIBASE_CONCEPT}/prop/statement/value/\": \"property\",
		\"${WIKIBASE_SCHEME}://${WIKIBASE_CONCEPT}/prop/statement/value-normalized/\": \"property\",
		\"${WIKIBASE_SCHEME}://${WIKIBASE_CONCEPT}/prop/qualifier/\": \"property\",
		\"${WIKIBASE_SCHEME}://${WIKIBASE_CONCEPT}/prop/qualifier/value/\": \"property\",
		\"${WIKIBASE_SCHEME}://${WIKIBASE_CONCEPT}/prop/qualifier/value-normalized/\": \"property\",
		\"${WIKIBASE_SCHEME}://${WIKIBASE_CONCEPT}/prop/reference/\": \"property\",
		\"${WIKIBASE_SCHEME}://${WIKIBASE_CONCEPT}/prop/reference/value/\": \"property\",
		\"${WIKIBASE_SCHEME}://${WIKIBASE_CONCEPT}/prop/reference/value-normalized/\": \"property\",
		\"${WIKIBASE_SCHEME}://${WIKIBASE_CONCEPT}/wiki/Special:EntityData/\": \"item\",
		\"${WIKIBASE_SCHEME}://${WIKIBASE_CONCEPT}/entity/\": \"item\"
	};

	RdfNamespaces.ALL_PREFIXES = $.map( RdfNamespaces.NAMESPACE_SHORTCUTS, function ( n ) {
		return n;
	} ).reduce( function ( p, v, i ) {
		return $.extend( p, v );
	}, {} );

	RdfNamespaces.STANDARD_PREFIXES = {




		tib: \"PREFIX tib: <${WIKIBASE_SCHEME}://${WIKIBASE_CONCEPT}/entity/>\",
		tibt: \"PREFIX tibt: <${WIKIBASE_SCHEME}://${WIKIBASE_CONCEPT}/prop/direct/>\",
		tibp: \"PREFIX tibp: <${WIKIBASE_SCHEME}://${WIKIBASE_CONCEPT}/prop/>\",
		tibps: \"PREFIX tibps: <${WIKIBASE_SCHEME}://${WIKIBASE_CONCEPT}/prop/statement/>\",
		tibpq: \"PREFIX tibpq: <${WIKIBASE_SCHEME}://${WIKIBASE_CONCEPT}/prop/qualifier/>\",
		wikibase: \"PREFIX wikibase: <http://wikiba.se/ontology#>\",
		rdfs: \"PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\",
		bd: \"PREFIX bd: <http://www.bigdata.com/rdf#>\"
	};

	RdfNamespaces.addPrefixes = function( prefixes ) {
		$.extend( RdfNamespaces.ALL_PREFIXES, prefixes );
	};

	RdfNamespaces.getPrefixMap = function ( entityTypes ) {
		var prefixes = {};
		$.each( RdfNamespaces.ALL_PREFIXES, function ( prefix, url ) {
			if ( entityTypes[url] ) {
				prefixes[prefix] = entityTypes[url];
			}
		} );
		return prefixes;
	};

} )( jQuery, wikibase.queryService.RdfNamespaces );
" > $1

echo "RdfNamespaces.js processed"
