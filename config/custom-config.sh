#!/bin/bash
# generate custom-config.json for custom-wdqs.

echo "
{
    \"api\": {
      \"sparql\": {
        \"uri\":  \"${WIKIBASE_SCHEME}://${CUSTOM_CONFIG_QUERY}/proxy/wdqs/bigdata/namespace/wdq/sparql\"
      },
      \"wikibase\": {
        \"uri\": \"${WIKIBASE_SCHEME}://${WIKIBASE_ADDRESS}/w/api.php\"
      },
      \"examples\": {
        \"server\": \"${WIKIBASE_SCHEME}://${WIKIBASE_ADDRESS}/\",
        \"apiPath\": \"w/api.php\",
        \"pageTitle\": \"Query/Examples\",
        \"pagePathElement\": \"wiki/\"
      },
      \"urlShortener\": \"tinyurl\",
      \"query-builder\": {
        \"server\": \"https://query-builder-test.toolforge.org/\"
      }
    },
    \"brand\": {
      \"title\": \"${WIKIBASE_NAME} Query Service\",
      \"logo\": \"\",
      \"favicon\": \"\",
      \"copyrightUrl\": \"\"
    },
    \"location\": {
      \"root\": \"./\",
      \"index\": \"./\"
    },
    \"prefixes\": {}
  }
" > $1

echo "custom-config.json processed"
echo "Here we can see which environment variables are available:"
echo "WIKIBASE_SCHEME: ${WIKIBASE_SCHEME}"
echo "WIKIBASE_ADDRESS: ${WIKIBASE_ADDRESS}"
echo "WIKIBASE_NAME: ${WIKIBASE_NAME}"
echo "CUSTOM_CONFIG_QUERY: ${CUSTOM_CONFIG_QUERY}"